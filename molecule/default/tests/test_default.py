import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('radius')


def test_radiusd_running(host):
    service = host.service("radiusd")
    assert service.is_running
    assert service.is_enabled
